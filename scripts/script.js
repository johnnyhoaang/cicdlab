/*
@Author Johnny Hoang [2036759]
This script creates a table with user inputs submitted by the button
*/
"use strict";

document.addEventListener("DOMContentLoaded", setUp);

//Sets up event listener for the button 
function setUp(){
    //Get a handle on the button
    let button = document.querySelector("#create");
    button.addEventListener('click', createTable);
}
//This function creates a table with given row-column numbers and odd-even colors
function createTable(e){
    //The statement deletes an existing table
    if (document.querySelector('table')){
        document.querySelector(table).textContent = undefined;
    }
    //Gets a handle on the row and column numbers from the inputs
    let rowNumber = document.querySelector("#row").value;
    let columnNumber = document.querySelector("#column").value;
    //Gets a handle on the colors from the inputs
    let evenColor  = document.querySelector("#evencolor").value;
    let oddColor = document.querySelector("#oddcolor").value;
    //Gets a handle on section for the table
    let tableLocation = document.querySelector("#tableHere")
    //Creating table element with an id, then adding table to the section
    let table = document.createElement('table');
    tableLocation.appendChild(table);
    table.id = "dynamictable"
    //Initializing a counter that will count the table grids
    let number = 0;
    //Loop creates table using the previous row and column inputs
        for(let i=0; i<rowNumber; i++){
            let tr = document.createElement("tr");
            for(let j=0; j<columnNumber; j++){
                let td = document.createElement("td");
                //The grid contains the index of i and j
                td.textContent=i + ", " + j;
                tr.appendChild(td);
            
            //The statement sets the table like a chess board with its respective colors
                if ((j+number) % 2== 0) {
                    td.style.backgroundColor=evenColor;
                }
                else if ((j+number) % 2 == 1) {
                    td.style.backgroundColor=oddColor;
                }
            }
            number=number+1;
        //Row with columns is added
        table.appendChild(tr);
        }
    //Prevents the inputs to get sent to the server
    e.preventDefault();
}
